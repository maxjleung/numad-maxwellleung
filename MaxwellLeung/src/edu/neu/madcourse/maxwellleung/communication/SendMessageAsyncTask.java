package edu.neu.madcourse.maxwellleung.communication;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import edu.neu.madcourse.maxwellleung.R;
import edu.neu.mhealth.api.KeyValueAPI;

public class SendMessageAsyncTask extends AsyncTask<String, Void, String> {
	private static final String TAG = "SendMessageAsyncTask";

	private Context context;

	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";

	String regid;
	String SENDER_ID = "397697658412";
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();

	public SendMessageAsyncTask(Context context, String regid) {
		this.context = context;
		this.regid = regid;
	}

	@Override
	protected String doInBackground(String... params) {
		String message = params[0];
		Boolean success;

		String msg = "";
		int registrations = 0;
		if (!KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "registrations").contains("Error"))
			registrations = Integer
					.parseInt(KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "registrations"));
		else {
			msg = KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "registrations", "1");
		}
		List<String> regIds = new ArrayList<String>();
		String reg_device = regid;
		int nIcon = R.drawable.icon;
		int nType = Communication_Globals.SIMPLE_NOTIFICATION;
		Map<String, String> msgParams;
		msgParams = new HashMap<String, String>();
		msgParams.put("data.alertText", "Notification");
		msgParams.put("data.titleText", "Notification Title");
		msgParams.put("data.contentText", message);
		msgParams.put("data.nIcon", String.valueOf(nIcon));
		msgParams.put("data.nType", String.valueOf(nType));
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "alertText",
				"Message Notification");
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "titleText", "Sending Message");
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "contentText", message);
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "nIcon", String.valueOf(nIcon));
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "nType", String.valueOf(nType));
		GCMNotification gcmNotification = new GCMNotification();
		for (int i = 1; i <= registrations; i++) {
			regIds.clear();
			reg_device = KeyValueAPI.get(TEAM_NAME, TEAM_PASS,
					"regid" + String.valueOf(i));
			Log.d(String.valueOf(i), reg_device);
			regIds.add(reg_device);
			gcmNotification.sendNotification(msgParams, regIds, context);
			Log.d(String.valueOf(i), regIds.toString());
		}
		msg = "sending information...";
		return msg;

//		try {
//			gcm = GoogleCloudMessaging.getInstance(context);
//			Bundle data = new Bundle();
//			data.putString("my_message", message);
//			data.putString("my_action",
//					"edu.neu.madcourse.maxwellleung.communication.ECHO_NOW");
//			String id = Integer.toString(msgId.incrementAndGet());
//			gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
//			message = "Sent message";
//			GCMNotification notification = new GCMNotification();
//			success = true;
//			Log.d(TAG, "success");
//		} catch (IOException ex) {
//			message = "Error :" + ex.getMessage();
//			success = false;
//		}
//		return msg;
	}

	protected void onPostExecute(Boolean result) {
		if (!result) {
			toastShort("Unable to send messages at this time.");
		} else {
			toastShort("Message Sent!");
		}
	}

	void toastShort(String text) {
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
