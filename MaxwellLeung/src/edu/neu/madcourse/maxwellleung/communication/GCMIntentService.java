/*
 * Copyright (c) 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package edu.neu.madcourse.maxwellleung.communication;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Application;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import edu.neu.madcourse.maxwellleung.R;
import edu.neu.madcourse.maxwellleung.communication.Consts;
import edu.neu.mhealth.api.KeyValueAPI;

import java.io.IOException;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GCMIntentService extends IntentService {
	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GCMIntentService() {
		super("GCMIntentService");
	}

	public static final String TAG = "GCMIntentService";

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "onHandleIntent");
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		String alertText;
		String titleText;
		String contentText;
		if (!extras.isEmpty()) { // has effect of unparcelling Bundle

//			alertText = extras.getString("alertText", "");
//			titleText = extras.getString("titleText", "");
//			contentText = extras.getString("contentText", "");

			alertText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "alertText");
			titleText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "titleText");
			contentText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "contentText");
		} else {
			alertText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "alertText");
			titleText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "titleText");
			contentText = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "contentText");
		}
		sendNotification(alertText, titleText, contentText);

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	public void sendNotification(String alertText, String titleText,
			String contentText) {
		Log.d(TAG, "sendNotification");
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Intent notificationIntent;
		notificationIntent = new Intent(this, Communication.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		notificationIntent.putExtra("show_response", "show_response");
		PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(
				this, Communication.class), PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.icon)
				.setContentTitle(titleText)
				.setStyle(
						new NotificationCompat.BigTextStyle()
								.bigText(contentText))
				.setContentText(contentText).setTicker(alertText)
				.setAutoCancel(true)
				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
		mBuilder.setContentIntent(intent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
}
