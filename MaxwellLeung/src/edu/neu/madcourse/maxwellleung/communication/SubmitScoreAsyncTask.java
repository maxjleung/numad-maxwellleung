package edu.neu.madcourse.maxwellleung.communication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import edu.neu.mhealth.api.KeyValueAPI;

public class SubmitScoreAsyncTask extends AsyncTask<Score, Void, Boolean> {
	
	private static final String NO_SUCH_KEY = "Error: No Such Key";
	private static final String HIGH_SCORES_KEY = "high_scores";
	private final String TAG = "ServerStorageTask";
	private Context context;

	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";

	private List<Score> highScores = new ArrayList<Score>();

	public SubmitScoreAsyncTask(Context context) {
		this.context = context;
	}

	@Override
	protected Boolean doInBackground(Score... params) {
		// TODO Auto-generated method stub
		if (KeyValueAPI.isServerAvailable()) {
			Log.i(TAG, "server is available");
			getHighScores();
			for (Score s : params) {
				addScore(s);
			}
		} else {
			Log.d(TAG, "server unavailable");
		}
		return isServerAvailable();
	}

	private void getHighScores() {
		Type listOfScoresObject = new TypeToken<List<Score>>() {
		}.getType();
		Gson gson = new Gson();

		try {
			String json = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, HIGH_SCORES_KEY);
			if(json.equals(NO_SUCH_KEY)){
				Log.d(TAG, "Key " + HIGH_SCORES_KEY +" was empty.");
				return;
			}
			Log.d(TAG, "json: "+json);
			highScores = gson.fromJson(json, listOfScoresObject);
		} catch (Exception e) {

		}
	}

	private void addScore(Score score) {
		// Add the score to the list
		highScores.add(score);
		Collections.sort(highScores);

		// Reserialize list
		Gson gson = new Gson();
		String json = gson.toJson(highScores);

		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, HIGH_SCORES_KEY, json);
	}

	private boolean isServerAvailable() {
		return KeyValueAPI.isServerAvailable();
	}

	protected void onProgressUpdate(Integer... progress) {
		// setProgressPercent(progress[0]);
	}

	protected void onPostExecute(Boolean result) {
		if (!result) {
			return;
		} else {
			toastComplete();
		}
	}

	public void toastComplete() {
		CharSequence text = "Score Submitted!";
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
