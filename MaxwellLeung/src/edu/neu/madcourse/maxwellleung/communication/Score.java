package edu.neu.madcourse.maxwellleung.communication;

import java.util.Comparator;

import com.google.gson.Gson;

public class Score implements Comparable<Score> {
	private String username = "Anonymous";
	private int score;

	public Score(String mUser, int mScore) {
		if(mUser == null){
			mUser = "Anonymous";
		}
		this.username = mUser;
		this.score = mScore;
	}

	public String getUser() {
		return username;
	}

	public void setUser(String user) {
		this.username = user;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String toJson() {
		Gson gson = new Gson();
		String json = gson.toJson(this);
		return json;
	}

	public static Score fromJson(String json) {
		Gson gson = new Gson();
		Score newScore = gson.fromJson(json, Score.class);
		return newScore;
	}


	public int compareTo(Score another) {
		return Integer.signum(another.getScore() -score );
	}
	
	@Override
	public String toString(){
		return username +" - " + score;
		
		
	}
}
