package edu.neu.madcourse.maxwellleung.communication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.InputType;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import edu.neu.mhealth.api.KeyValueAPI;

public class GetScoresAsyncTask extends AsyncTask<Void, Void, Boolean> {
	private final String TAG = "ServerStorageTask";

	private static final String NO_SUCH_KEY = "Error: No Such Key";
	private static final String HIGH_SCORES_KEY = "high_scores";
	private Context context;

	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";

	private List<Score> highScores = new ArrayList<Score>();

	public GetScoresAsyncTask(Context context) {
		this.context = context;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		if (KeyValueAPI.isServerAvailable()) {
			Log.i(TAG, "server is available");
			getHighScores();
		} else {
			Log.d(TAG, "server unavailable");
		}
		return isServerAvailable();
	}

	private void getHighScores() {
		Type listOfScoresObject = new TypeToken<List<Score>>() {
		}.getType();
		Gson gson = new Gson();

		String json = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, HIGH_SCORES_KEY);
		if(json.equals(NO_SUCH_KEY)){
			Log.d(TAG, "Key " + HIGH_SCORES_KEY +" was empty.");
			//highScores.add(null);
			return;
		}
		highScores = gson.fromJson(json, listOfScoresObject);
		Collections.sort(highScores);
	}

	private boolean isServerAvailable() {
		return KeyValueAPI.isServerAvailable();
	}

	protected void onProgressUpdate(Integer... progress) {
		// setProgressPercent(progress[0]);
	}

	protected void onPostExecute(Boolean result) {
		if (!result) {
			return;
		} else {
			showScores();
		}
	}

	public void showScores() {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		// Set up the input
		final ListView scoreList = new ListView(context);
		builder.setTitle("High Scores");
		ArrayAdapter<Score> adapter = new ArrayAdapter<Score>(context,
				android.R.layout.simple_list_item_1, highScores);
		scoreList.setAdapter(adapter);
		builder.setView(scoreList);

		builder.show();
	}
}
