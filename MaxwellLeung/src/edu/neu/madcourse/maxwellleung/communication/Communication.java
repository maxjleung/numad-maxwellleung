package edu.neu.madcourse.maxwellleung.communication;

//import com.google.cloud.backend.core.CloudBackendFragment.OnListener;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import edu.neu.madcourse.maxwellleung.R;
import edu.neu.mhealth.api.KeyValueAPI;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class Communication extends Activity implements OnClickListener {

	private static final String TAG = "Communication";
	/*
	 * UI components
	 */
	private TextView mEmptyView;
	private EditText mMessageTxt;
	private TextView mUsernameView;
	private TextView mScoreView;
	private ImageView mSendBtn;
	private TextView mAnnounceTxt;
	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";

	private String username;
	protected String mUser;
	protected int mScore;

	/*
	 * GCM consts
	 */
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	String SENDER_ID = "397697658412";
	String regid;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_communication);
		
		
		mEmptyView = (TextView) findViewById(R.id.communication_display);

		View userBtn = findViewById(R.id.set_user_button);
		userBtn.setOnClickListener(this);

		View scoreBtn = findViewById(R.id.set_score_button);
		scoreBtn.setOnClickListener(this);
		View submitBtn = findViewById(R.id.submit_score_button);
		submitBtn.setOnClickListener(this);
		View viewBtn = findViewById(R.id.view_score_button);
		viewBtn.setOnClickListener(this);
		View msgBtn = findViewById(R.id.send_hello_button);
		msgBtn.setOnClickListener(this);

		View acksBtn = findViewById(R.id.communication_acks_button);
		acksBtn.setOnClickListener(this);

		mUsernameView = (TextView) findViewById(R.id.communication_user);
		mScoreView = (TextView) findViewById(R.id.communication_score);

		/*
		 * Setup GCM
		 */
		context = getApplicationContext();

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			Log.i(TAG, "RegID: " + regid);
			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.set_user_button:
			setUserDialog();
			break;
		case R.id.set_score_button:
			setScoreDialog();
			break;
		case R.id.submit_score_button:
			submitScore();
			break;
		case R.id.view_score_button:
			viewScores();
			break;
		case R.id.send_hello_button:
			setMessageDialog();
			break;
		case R.id.communication_acks_button:
			Intent s = new Intent(this, Acks.class);
			startActivity(s);
			break;
		default:
			break;
		}
	}

	void setUserDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Set up the input
		final EditText input = new EditText(this);
		builder.setTitle("Set Username");
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mUser = input.getText().toString();
				mUsernameView.setText("Username: " + mUser);
			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	void setScoreDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected;
		input.setInputType(InputType.TYPE_CLASS_PHONE);
		builder.setView(input);
		builder.setTitle("Set Score");
		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int score = 0;
				try {
					score = Integer.parseInt(input.getText().toString());
				} catch (NumberFormatException e) {
					score = 0;
				}
				mScore = score;
				mScoreView.setText("SCORE: " + mScore);
			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	void setMessageDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Set up the input
		final EditText input = new EditText(this);
		builder.setTitle("Write message to send");
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String msg = input.getText().toString();
				if (msg.isEmpty()) {
					return;
				}
				SendMessageAsyncTask task = new SendMessageAsyncTask(
						getApplicationContext(), regid);
				task.execute(msg);
				toastShort("Sending Message..");
			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	void submitScore() {
		toastShort("Submitting Score..");
		SubmitScoreAsyncTask task = new SubmitScoreAsyncTask(this);
		Score s = new Score(mUser, mScore);
		task.execute(s);
	}

	void viewScores() {
		GetScoresAsyncTask task = new GetScoresAsyncTask(this);
		task.execute();
	}

	void toastShort(String text) {
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this, text, duration);
		toast.show();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGcmPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Gets the current registration ID for application on GCM service, if there
	 * is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGcmPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "alertText",
							"Register Notification");
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "titleText",
							"Register");
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "contentText",
							"Registering Successful!");

					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;
					Log.d(TAG, msg);

					int registrations = 0;
					if (KeyValueAPI.isServerAvailable()) {
						if (!KeyValueAPI.get(TEAM_NAME, TEAM_PASS,
								"registrations").contains("Error")) {
							registrations = Integer.parseInt(KeyValueAPI.get(
									TEAM_NAME, TEAM_PASS, "registrations"));
						} else {
							KeyValueAPI.put(TEAM_NAME, TEAM_PASS,
									"registrations", "1");
						}
						String getString;
						boolean alreadyRegistered = false;
						for (int i = 1; i <= registrations; i++) {
							getString = KeyValueAPI.get(TEAM_NAME, TEAM_PASS,
									"regid" + String.valueOf(i));
							Log.d(String.valueOf(i), getString);
							if (getString.equals(regid))
								alreadyRegistered = true;
						}
						if (!alreadyRegistered) {
							KeyValueAPI.put(TEAM_NAME, TEAM_PASS,
									"registrations",
									String.valueOf(registrations + 1));
							KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "regid"
									+ String.valueOf(registrations + 1), regid);
						}
						msg = "Device registered, registration ID=" + regid;
					} else {
						msg = "Error :" + "Backup Server is not available";
						return msg;
					}
					// You should send the registration ID to your server over
					// HTTP, so it
					// can use GCM/HTTP or CCS to send messages to your app.
					sendRegistrationIdToBackend();

					// For this demo: we don't need to send it because the
					// device will send
					// upstream messages to a server that echo back the message
					// using the
					// 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}

	protected void sendRegistrationIdToBackend() {
		//
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(Communication.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}
}
