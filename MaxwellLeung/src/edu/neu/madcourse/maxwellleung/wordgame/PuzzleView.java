package edu.neu.madcourse.maxwellleung.wordgame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import edu.neu.madcourse.maxwellleung.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

public class PuzzleView extends View {

	private static final String TAG = "WordGame";

	private static final String SELX = "selX";
	private static final String SELY = "selY";
	private static final String VIEW_STATE = "viewState";

	private float width; // width of one tile
	private float height; // height of one tile
	private int selX; // X index of selection
	private int selY; // Y index of selection
	private final Rect selRect = new Rect();

	private static final ArrayList<Rect> SELECTION_PATH = new ArrayList<Rect>();
	private Game game;

	public PuzzleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		Log.d(TAG, "constructor atts");
	}

	private void init(Context context) {

		this.game = (Game) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable p = super.onSaveInstanceState();
		Log.d(TAG, "onSaveInstanceState");
		Bundle bundle = new Bundle();
		bundle.putInt(SELX, selX);
		bundle.putInt(SELY, selY);
		bundle.putParcelable(VIEW_STATE, p);
		return bundle;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Log.d(TAG, "onRestoreInstanceState");
		Bundle bundle = (Bundle) state;
		selectOrDeselect(bundle.getInt(SELX), bundle.getInt(SELY));
		super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = w / 9f;
		height = h / 9f;
		getRect(selX, selY, selRect);
		Log.d(TAG, "onSizeChanged: width " + width + ", height " + height);
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// Draw the background...
		super.onDraw(canvas);
		Paint background = new Paint();
		background.setColor(getResources().getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), background);

		// Draw the board...

		// Define colors for the grid lines
		Paint dark = new Paint();
		dark.setColor(getResources().getColor(R.color.puzzle_dark));

		Paint hilite = new Paint();
		hilite.setColor(getResources().getColor(R.color.puzzle_hilite));

		Paint light = new Paint();
		light.setColor(getResources().getColor(R.color.puzzle_light));

		// Draw the minor grid lines
		for (int i = 0; i < 9; i++) {
			canvas.drawLine(0, i * height, getWidth(), i * height, light);
			canvas.drawLine(0, i * height + 1, getWidth(), i * height + 1,
					hilite);
			canvas.drawLine(i * width, 0, i * width, getHeight(), light);
			canvas.drawLine(i * width + 1, 0, i * width + 1, getHeight(),
					hilite);
		}

		// Draw letters
		Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
		foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
		foreground.setStyle(Style.FILL);
		foreground.setTextSize(height * 0.75f);
		foreground.setTextScaleX(width / height);
		foreground.setTextAlign(Paint.Align.CENTER);

		// Draw the letter in the center of the tile
		FontMetrics fm = foreground.getFontMetrics();
		// Centering in X: use alignment (and X at midpoint)
		float x = width / 2;
		// Centering in Y: measure ascent/descent first
		float y = height / 2 - (fm.ascent + fm.descent) / 2;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				canvas.drawText(this.game.getTileString(i, j), i * width + x, j
						* height + y, foreground);
			}
		}

		// Draw the selection...
		for (Iterator<Rect> i = SELECTION_PATH.iterator(); i.hasNext();) {
			Rect rectToDraw = i.next();
			Log.d(TAG, "selRect=" + rectToDraw);
			Paint selectedPaint = new Paint();
			selectedPaint.setColor(getResources().getColor(
					R.color.puzzle_selected));
			canvas.drawRect(rectToDraw, selectedPaint);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() != MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);

		selectOrDeselect((int) (event.getX() / width),
				(int) (event.getY() / height));
		Log.d(TAG, "onTouchEvent: x " + selX + ", y " + selY);
		return true;
	}

	public void setSelectedTile(char tile) {
		game.setTile(selX, selY, tile);
		invalidate();

		Log.d(TAG, "setSelectedTile: invalid: " + tile);
		startAnimation(AnimationUtils.loadAnimation(game, R.anim.shake));
	}

	private void selectOrDeselect(int x, int y) {
		invalidate(selRect);
		selX = Math.min(Math.max(x, 0), 8);
		selY = Math.min(Math.max(y, 0), 8);
		Rect newSel = new Rect();
		getRect(selX, selY, newSel);
		if (SELECTION_PATH.isEmpty()) {
			if (!this.game.isTileBlank(this.game.getTile(selX, selY))) {
				SELECTION_PATH.add(newSel);
				game.selectTile(selX, selY);
			}
		} else {
			int lastIndex = SELECTION_PATH.size() - 1;
			Rect lastRect = SELECTION_PATH.get(lastIndex);
			if (lastRect.equals(newSel)) {
				// De-select letter
				game.deselectTile(selX, selY);
				SELECTION_PATH.remove(lastIndex);
				Log.d(TAG, "select: deselect");
			} else {
				if (!SELECTION_PATH.contains(newSel)) {
					// Not yet selected
					if (!this.game.isTileBlank(this.game.getTile(selX, selY))) {
						SELECTION_PATH.add(newSel);

						game.selectTile(selX, selY);
					}
				}
			}
		}
		invalidate(newSel);
	}

	protected void clearSelection() {
		invalidateSelection();
		SELECTION_PATH.clear();
	}

	protected void invalidateSelection() {
		for (Iterator<Rect> i = SELECTION_PATH.iterator(); i.hasNext();) {
			Rect r = i.next();
			invalidate(r);
		}
	}

	private void getRect(int x, int y, Rect rect) {
		rect.set((int) (x * width), (int) (y * height),
				(int) (x * width + width), (int) (y * height + height));
	}

	// ...
}
