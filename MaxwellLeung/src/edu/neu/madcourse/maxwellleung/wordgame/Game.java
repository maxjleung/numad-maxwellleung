package edu.neu.madcourse.maxwellleung.wordgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import edu.neu.madcourse.maxwellleung.R;
import edu.neu.madcourse.maxwellleung.sudoku.Keypad;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Game extends Activity implements OnClickListener {

	private static final String TAG = "WordGame";

	private String[] DICTIONARY = {};

	public static final String KEY_CONTINUE = "edu.neu.madcourse.maxwellleung.wordgame.difficulty";
	public static final int DEFAULT_PUZZLE = 0;
	private static final String PREFS_NAME = "WordGamePrefs";

	protected int GRID_ROWS = 9;
	protected int GRID_COLS = 9;

	protected static final int CONTINUING = -1;

	protected static int DROP_RATE = 12;

	protected static final int DIFFICULTY_INCREASE_INTERVAL = 30;
	private static final int DIFFICULTY_INCREASE_RATE = 1;

	private char[] puzzle = new char[82];
	private int score = 0;

	private String selection = "";
	private ArrayList<Pair> selectionTiles = new ArrayList<Pair>();

	private Vibrator vibrator;
	private PuzzleView puzzleView;
	private TextView selectionView;
	private TextView timerView;
	private long START_TIME_MS = 180000;
	private long TIME_REMAINING = START_TIME_MS;
	private long TIME_ELASPED = 0;
	private long TICKRATE = 200;
	private int COUNTDOWN_TIME = 12;

	private Set<String> potentialResults = new TreeSet<String>();
	private ArrayList<String> wordsFound = new ArrayList<String>();
	// runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {

		@Override
		public void run() {
			long millis = START_TIME_MS - TIME_ELASPED;
			TIME_REMAINING -= TICKRATE;
			TIME_ELASPED += TICKRATE;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds = seconds % 60;

			if (seconds > COUNTDOWN_TIME && minutes >= 0) {
				timerView.setText(String.format("%d:%02d", minutes, seconds));
				// Increase the rate of which tiles drop
				if (seconds % DIFFICULTY_INCREASE_INTERVAL == 0) {
					if (DROP_RATE > DIFFICULTY_INCREASE_RATE) {
						DROP_RATE -= DIFFICULTY_INCREASE_RATE;
					}
				}
				if (seconds % DROP_RATE == 0) {
					setRandomFreeTile();
				}
			}
			// start countdown music
			if (TIME_REMAINING == TICKRATE * COUNTDOWN_TIME) {
				Music.stop(getApplicationContext());
				Music.playOnce(getApplicationContext(), R.raw.timeout);
			}

			// blink the timer
			if (seconds <= COUNTDOWN_TIME && minutes == 0) {
				if (timerView.getText().toString().equals("")) {
					timerView.setText(String
							.format("%d:%02d", minutes, seconds));
				} else {
					timerView.setText("");
				}
			}
			if (TIME_REMAINING <= 0) {
				Win();
			}
			timerHandler.postDelayed(this, TICKRATE);
		}

	};

	private void gameOver() {
		Intent i = new Intent(this, GameOver.class);
		i.putExtra("win", false);
		i.putExtra("score", score);
		startActivity(i);
		// Clear the prefs
		getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
		finish();
	}

	private void Win() {
		Intent i = new Intent(this, GameOver.class);
		i.putExtra("win", true);
		i.putExtra("score", score);
		// Clear the prefs
		getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
		finish();
		startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		setContentView(R.layout.activity_wordgame_puzzle);

		puzzleView = (PuzzleView) findViewById(R.id.puzzleview);
		selectionView = (TextView) findViewById(R.id.selection);

		View clearButton = findViewById(R.id.clear_selection_button);
		clearButton.setOnClickListener((OnClickListener) this);
		Button pauseButton = (Button) findViewById(R.id.pause_button);
		pauseButton.setOnClickListener(this);
		Button submitButton = (Button) findViewById(R.id.submit_button);
		submitButton.setOnClickListener(this);

		// Set up timer
		timerView = (TextView) findViewById(R.id.timer);

		// Setup selection text listener

		selectionView.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable e) {

				// Case insensitive and remove any spaces
				String textInput = e.toString().toLowerCase().replace(" ", "");
				int inputLength = textInput.length();
				if (inputLength == 1) {
					if (Character.isLetter(textInput.charAt(0))) {
						try {
							loadDictionary(textInput.charAt(0));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
				// Do not show anything if 2 or less length
				if (inputLength > 2) {
					if (Arrays.asList(DICTIONARY).contains(textInput)) {
						potentialResults.add(textInput);
						Music.playOnce(getApplicationContext(), R.raw.beep);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}
		});
		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Music.play(this, R.raw.wordgame);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME,
				MODE_PRIVATE);
		int continuing = settings.getInt(KEY_CONTINUE, DEFAULT_PUZZLE);
		switch (continuing) {
		case CONTINUING:

			score = settings.getInt("score", 0);
			TIME_REMAINING = settings.getLong("timeremaining", START_TIME_MS);
			TIME_ELASPED = settings.getLong("timeelasped", 0);

			puzzle = getPuzzle(CONTINUING);
			break;
		case DEFAULT_PUZZLE:

			score = 0;
			TIME_REMAINING = START_TIME_MS;
			TIME_ELASPED = 0;

			puzzle = getPuzzle(DEFAULT_PUZZLE);
		default:
			puzzle = getPuzzle(CONTINUING);
		}

		resumeTimer();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_selection_button:
			clearSelection();
			break;
		case R.id.pause_button:
			pauseOrResumeTimer();
			break;
		case R.id.submit_button:
			submitSelection();
		}
	}

	private void submitSelection() {
		if (potentialResults.contains(selection.toLowerCase())) {
			char[] word = selection.toCharArray();
			addScore(getWordValue(word));
			Music.playOnce(this, R.raw.cash);
			destroySelectedTiles();
			adjustTiles();
			clearSelection();
		} else {
			// incorrect
			Music.playOnce(this, R.raw.wrong);
		}

	}

	private void adjustTiles() {
		for (int colNum = 0; colNum < 9; colNum++) {
			String column = new String(getColumn(colNum));
			// Remove blank spaces from the column
			column = column.replaceAll("\\s", "");
			// Create a blank column
			char[] newCol = new char[9];
			Arrays.fill(newCol, ' ');

			// Copy the remaining letters to the "bottom" of the column
			System.arraycopy(column.toCharArray(), 0, newCol,
					9 - column.length(), column.length());
			setColumn(newCol, colNum);
		}

	}

	private void destroySelectedTiles() {
		for (Iterator<Pair> i = selectionTiles.iterator(); i.hasNext();) {
			Pair<Integer, Integer> tile = i.next();
			setTile(tile.first, tile.second, ' ');
		}
	}

	private void addScore(int value) {
		score += value;
		TextView scoreView = (TextView) findViewById(R.id.score);
		scoreView.setText("SCORE: " + score);
	}

	private int getWordValue(char[] word) {
		int value = 0;
		for (int i = 0; i < word.length; i++) {
			value += getLetterValue(word[i]);
		}
		return value;
	}

	// Scrabble letter values
	private int getLetterValue(char letter) {

		int values[] = { 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1,
				1, 1, 1, 4, 4, 8, 4, 10 };

		return values[(int) letter - 'A'];
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		Music.stop(this);

		pauseTimer();
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences(PREFS_NAME,
				MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("savedpuzzle", new String(puzzle));
		editor.putInt("score", score);
		editor.putLong("timeremaining", TIME_REMAINING);
		editor.putLong("timeelasped", TIME_ELASPED);
		editor.putBoolean("continue", true);
		editor.commit();
	}

	private void pauseTimer() {
		Button pauseButton = (Button) findViewById(R.id.pause_button);
		pauseButton.setText("RESUME");
		timerHandler.removeCallbacks(timerRunnable);
		puzzleView.setVisibility(View.INVISIBLE);

	}

	private void resumeTimer() {
		Button pauseButton = (Button) findViewById(R.id.pause_button);
		pauseButton.setText("PAUSE");
		timerHandler.postDelayed(timerRunnable, 0);
		puzzleView.setVisibility(View.VISIBLE);

	}

	private void pauseOrResumeTimer() {

		Button pauseButton = (Button) findViewById(R.id.pause_button);
		if (pauseButton.getText().equals("PAUSE")) {
			pauseTimer();
		} else {
			resumeTimer();
		}
	}

	/** Given a difficulty level, come up with a new puzzle */
	private char[] getPuzzle(int diff) {
		String puz;
		switch (diff) {
		case CONTINUING:
			SharedPreferences settings = getSharedPreferences(PREFS_NAME,
					MODE_PRIVATE);
			Log.d(TAG, "onPause: put PREF_PUZZLE getting\n" + puzzle);
			puz = settings.getString("savedpuzzle", makeNewPuzzle());
			break;
		default:
			puz = makeNewPuzzle();
			break;
		}

		return puz.toCharArray();
	}

	private String makeNewPuzzle() {
		for (int i = 0; i < 54; i++) {
			puzzle[i] = ' ';
		}
		for (int j = 54; j < 81; j++) {

			puzzle[j] = generateLetter();
		}
		return new String(puzzle);
	}

	// Generate a random letter with some weight towards vowels
	private char generateLetter() {
		char[] vowels = { 'A', 'E', 'I', 'O', 'U' };
		Random r = new Random();
		char c = ' ';
		if (r.nextInt(5) == 0) { // Generate a vowel
			c = vowels[r.nextInt(5)];
		} else {
			c = (char) (r.nextInt(26) + 'A'); // Generate a random letter
		}
		return c;
	}

	/** Convert an array into a puzzle string */
	static private String toPuzzleString(char[] puz) {
		StringBuilder buf = new StringBuilder();
		for (int element : puz) {
			buf.append(element);
		}
		return buf.toString();
	}

	/** Convert a puzzle string into an array */
	static protected char[] fromPuzzleString(String puzzleString) {
		char[] puz = new char[puzzleString.length()];
		for (int i = 0; i < puz.length; i++) {
			puz[i] = puzzleString.charAt(i);
		}
		return puz;
	}

	/** Return the tile at the given coordinates */
	protected char getTile(int x, int y) {
		return puzzle[y * 9 + x];
	}

	/** Return the entire column ordered top-down at the given column */
	private char[] getColumn(int col) {
		char[] column = new char[9];
		for (int i = 0; i < 9; i++) {
			column[i] = getTile(col, i);
		}
		return column;
	}

	private void setColumn(char[] column, int columnNum) {
		for (int i = 0; i < 9; i++) {
			char value = column[i];
			setTile(columnNum, i, value);
		}
	}

	/** Change the tile at the given coordinates */
	protected void setTile(int x, int y, char value) {
		puzzle[y * 9 + x] = value;
		puzzleView.invalidate();
	}

	/** Return a string for the tile at the given coordinates */
	protected String getTileString(int x, int y) {
		return getTile(x, y) + "";
	}

	protected void clearSelection() {
		selection = "";
		puzzleView.clearSelection();
		selectionView.setText(selection);
		selectionTiles.clear();
	}

	protected void deselectTile(int selX, int selY) {
		vibrator.vibrate(100);
		if (selection.length() == 1 || selection.length() == 0) {
			selection = "";
			selectionTiles.clear();
		} else {
			// Remove last selected tile
			selection = selection.substring(0, selection.length() - 1);
			selectionTiles.remove(selectionTiles.size() - 1);
		}
		selectionView.setText(selection);
	}

	protected void selectTile(int selX, int selY) {
		vibrator.vibrate(100);
		char tile = getTile(selX, selY);
		if (!(tile == ' ')) {
			selection += tile;
			selectionTiles.add(new Pair<Integer, Integer>(selX, selY));
			selectionView.setText(selection);
		}
	}

	// Assigns a letter to a random lowest free tile
	private void setRandomFreeTile() {
		Random r = new Random();
		int columnNum=0;
		int lowestSpace=0;
		do {
			// Check if lost
			int tilesUsed = new String(puzzle).trim().length();
			int tilesMax = puzzle.length-1;
			if (tilesUsed == tilesMax) {
				gameOver();
				break;
			}
			columnNum = r.nextInt(9);
			lowestSpace = lowestFreeSpace(columnNum);
		} while (lowestSpace == -1);

		setTile(columnNum, lowestSpace, generateLetter());
	}

	protected boolean isTileBlank(char tile) {
		return tile == ' ';
	}

	// Find the "lowest" open tile in the given column
	private int lowestFreeSpace(int columnNum) {
		for (int i = 8; i > -1; i--) {
			if (isTileBlank(getTile(columnNum, i))) {
				return i;
			}
		}
		return -1;
	}

	private void loadDictionary(char letter) throws IOException {

		AssetManager assetManager = getAssets();
		try {

			ArrayList<String> inputList = new ArrayList<String>();
			String filename = "wordlist_" + letter + ".txt";
			InputStream input = assetManager.open(filename);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					input, "UTF-8"));
			String nextLine;
			try {
				while ((nextLine = reader.readLine()) != null) {
					inputList.add(nextLine);
				}
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			if (inputList.isEmpty()) {
				throw new IOException("List was empty when reading from file");
			}
			input.close();
			reader.close();

			DICTIONARY = inputList.toArray(new String[inputList.size()]);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
