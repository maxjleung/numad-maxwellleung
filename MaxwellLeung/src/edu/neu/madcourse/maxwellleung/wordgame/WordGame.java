package edu.neu.madcourse.maxwellleung.wordgame;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import edu.neu.madcourse.maxwellleung.R;
import edu.neu.madcourse.maxwellleung.sudoku.AboutSudoku;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import edu.neu.mobileClass.*;

public class WordGame extends Activity implements OnClickListener {
	private static final String TAG = "WordGame";
	private static final String PREFS_NAME = "WordGamePrefs";

	public static final String KEY_CONTINUE = "edu.neu.madcourse.maxwellleung.wordgame.difficulty";
	private static final int DEFAULT_PUZZLE = 0;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wordgame);

		// Set up click listeners for all the buttons
		View continueButton = findViewById(R.id.continue_button);
		continueButton.setOnClickListener(this);
		View newButton = findViewById(R.id.new_button);
		newButton.setOnClickListener(this);

		View multiplayer = findViewById(R.id.multiplayer_button);
		multiplayer.setOnClickListener(this);

		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
		View acksButton = findViewById(R.id.wordgame_acks_button);
		acksButton.setOnClickListener(this);
		
		   // Check device for Play Services APK.
	    if (checkPlayServices()) {
	        // If this check succeeds, proceed with normal processing.
	        // Otherwise, prompt user to get valid Play Services APK.
	    }
	}

	@Override
	protected void onResume() {
		super.onResume();

	    checkPlayServices();
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		boolean isContinuing = settings.getBoolean("continue", false);
		View continueButton = findViewById(R.id.continue_button);
		continueButton.setOnClickListener(this);

		if (!isContinuing) {
			continueButton.setVisibility(View.INVISIBLE);
		} else {
			continueButton.setVisibility(View.VISIBLE);
		}
		Music.play(this, R.raw.wordgamemain);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Music.stop(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.continue_button:
			startGame(Game.CONTINUING);
			break;
		case R.id.new_button:
			startGame(Game.DEFAULT_PUZZLE);
			break;
		case R.id.multiplayer_button:
			//new ServerStorageTask().execute("maxjleung", "maxwell", "test");
			Intent x = new Intent(this, MultiplayerLobby.class);
			startActivity(x);
			break;
		case R.id.wordgame_acks_button:
			Intent i = new Intent(this, Acks.class);
			startActivity(i);
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			startActivity(new Intent(this, Prefs.class));
			return true;
			// More items go here (if any) ...
		}
		return false;
	}

	/** Start a new game with the given difficulty level */
	private void startGame(int i) {
		Log.d(TAG, "clicked on " + i);
		Intent intent = new Intent(this, Game.class);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME,
				MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(KEY_CONTINUE, i);
		editor.commit();
		startActivity(intent);
	}
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}
}