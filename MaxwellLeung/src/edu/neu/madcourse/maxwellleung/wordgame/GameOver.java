package edu.neu.madcourse.maxwellleung.wordgame;

import edu.neu.madcourse.maxwellleung.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class GameOver extends Activity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wordgame_winlose);

		View okButton = findViewById(R.id.winlose_ok);
		okButton.setOnClickListener(this);
		
		Intent i = getIntent();
		int score = i.getIntExtra("score", 0);
		TextView scoreView = (TextView) findViewById(R.id.winlose_score);
		scoreView.setText("\nScore: "+(score));

		boolean win = i.getBooleanExtra("win", true);
		if (win) {
			Win();
		} else {
			Lose();
		}

	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.winlose_ok:
			finish();
			break;
		}
	}
	protected void Win() {
		TextView winlose = (TextView) findViewById(R.id.winlose_content);
		winlose.setText("You Win!");
	}

	protected void Lose() {
		TextView winlose = (TextView) findViewById(R.id.winlose_content);
		winlose.setText("You Lose!");
	}

}
