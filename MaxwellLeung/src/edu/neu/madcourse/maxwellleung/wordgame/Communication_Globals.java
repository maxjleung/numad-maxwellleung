package edu.neu.madcourse.maxwellleung.wordgame;

public class Communication_Globals
{
    public static final String TAG = "GCM_Globals";
    public static final String GCM_SENDER_ID = "397697658412";
    public static final String BASE_URL = "https://android.googleapis.com/gcm/send";
    public static final String PREFS_NAME = "GCM_Communication";
    public static final String GCM_API_KEY = "AIzaSyCcfA12zz1ihLKTbW90sqgbxd64GxHApJA";
    public static final int SIMPLE_NOTIFICATION = 22;
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
    public static int mode = 0;
}