package edu.neu.madcourse.maxwellleung.wordgame;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import edu.neu.madcourse.maxwellleung.R;
import edu.neu.mhealth.api.KeyValueAPI;

public class SendInvitationAsyncTask extends AsyncTask<String, Void, String> {
	private static final String TAG = "SendMessageAsyncTask";

	private Context context;

	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";

	String SENDER_ID = "397697658412";
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	String regId;

	public SendInvitationAsyncTask(Context context, String regid) {
		this.context = context;
		this.regId = regid;
	}

	@Override
	protected String doInBackground(String... params) {
		String toUser = params[0];
		String fromUser = params[1];
		String message = "Invitation from " + fromUser;
		Boolean success;
		List<String> regIds = new ArrayList<String>();
		GCMNotification gcmNotification = new GCMNotification();

		String msg = "";
		String storedUser = KeyValueAPI.get(TEAM_NAME, TEAM_PASS, toUser);
		if (storedUser.contains("Error") || storedUser.isEmpty()) {
			message = "User named " + toUser + " does not exist!";
			regIds.add(regId);

			Map<String, String> msgParams = setupNotification(message);
			gcmNotification.sendNotification(msgParams, regIds, context);
			
			Log.d(TAG, message);
			return msg;
		} else {
			//Send inv to target
			regIds.clear();
			regIds.add(storedUser);
			
			Map<String, String> targetParams = setupNotification(message);
			gcmNotification.sendNotification(targetParams, regIds, context);
			
			//Send notification to sender
			String sentMsg = "Invitation sent to " + toUser;
			regIds.clear();
			regIds.add(regId);

			Map<String, String> senderParams = setupNotification(sentMsg);
			gcmNotification.sendNotification(senderParams, regIds, context);
			Log.d(TAG, sentMsg);
			
		}

		
		msg = "sending information...";
		return msg;

	}

	private Map<String, String> setupNotification(String message) {
		int nIcon = R.drawable.icon;
		int nType = Communication_Globals.SIMPLE_NOTIFICATION;
		

		Map<String, String> msgParams = new HashMap<String, String>();
		msgParams.put("data.alertText", "Notification");
		msgParams.put("data.titleText", "Notification Title");
		msgParams.put("data.contentText", message);
		msgParams.put("data.nIcon", String.valueOf(nIcon));
		msgParams.put("data.nType", String.valueOf(nType));
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "titleText", "Sending Message");
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "contentText", message);
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "nIcon", String.valueOf(nIcon));
		KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "nType", String.valueOf(nType));
		return msgParams;
	}

	protected void onPostExecute(Boolean result) {
		if (!result) {
			toastShort("Unable to send messages at this time.");
		} else {
			toastShort("Message Sent!");
		}
	}

	void toastShort(String text) {
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
