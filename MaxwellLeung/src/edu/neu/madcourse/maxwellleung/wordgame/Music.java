/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/

package edu.neu.madcourse.maxwellleung.wordgame;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class Music {

	private static MediaPlayer musicplayer = null;

	private static MediaPlayer soundplayer = null;

	/** Stop old song and start new one */

	public static void play(Context context, int resource) {
		stop(context);

		// Start music only if not disabled in preferences
		if (Prefs.getMusic(context)) {
			musicplayer = MediaPlayer.create(context, resource);
			musicplayer.setLooping(true);
			musicplayer.start();
		}
	}

	public static void playOnce(Context context, int resource) {
		// Start music only if not disabled in preferences
		if (Prefs.getMusic(context)) {
			soundplayer = MediaPlayer.create(context, resource);
			soundplayer.setLooping(false);
			soundplayer.start();
		}
	}

	/** Stop the music */
	public static void stop(Context context) {
		if (musicplayer != null) {
			musicplayer.stop();
			musicplayer.release();
			musicplayer = null;
		}
	}
}
