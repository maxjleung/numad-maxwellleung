package edu.neu.madcourse.maxwellleung.wordgame;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.ConnectionResult;

import edu.neu.madcourse.maxwellleung.R;
import edu.neu.madcourse.maxwellleung.communication.SendMessageAsyncTask;
import edu.neu.mhealth.api.KeyValueAPI;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MultiplayerLobby extends Activity implements OnClickListener {

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String PREFS_NAME = "WordGamePrefs";
	private static final String USERNAME = "username";

	private static final String OPPONENT = "opponent";
	private static final String OPPONENT_REG_ID = "opponentid";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private String TEAM_NAME = "maxjleung";
	private String TEAM_PASS = "maxwell";
	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "397697658412";

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "Multiplayer";

	TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	Context context;

	String username = "";
	String opponent_id = "";
	String opponent = "";
	String regid = "";
	Boolean waitingForOpponent = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_multiplayer);
		mDisplay = (TextView) findViewById(R.id.multiplayer_display);

		View invBtn = findViewById(R.id.multiplayer_send_invite);
		invBtn.setOnClickListener(this);

		View startBtn = findViewById(R.id.multiplayer_start);
		startBtn.setOnClickListener(this);

		context = getApplicationContext();

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);
			username = getUsername(context);
			if (regid.isEmpty()) {
				registerInBackground();
			}
			if (username.isEmpty()) {
				setUserDialog();
			} else {
				TextView mUsernameView = (TextView) findViewById(R.id.multiplayer_user);
				mUsernameView.setText("Username: " + username);

				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username
								+ "_READY", "false");
						return null;
					}
				}.execute(null, null, null);
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	// You need to do the Play Services APK check here too.
	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	private String getUsername(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String user = prefs.getString(USERNAME, "");
		if (user.isEmpty()) {
			Log.i(TAG, "Username not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return user;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "alertText",
							"Register Notification");
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "titleText",
							"Register");
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "contentText",
							"Registering Successful!");

					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;
					Log.d(TAG, msg);

					int registrations = GetNumberOfRegistrations();
					RegisterIfUnregistered(registrations);
					// You should send the registration ID to your server over
					// HTTP, so it
					// can use GCM/HTTP or CCS to send messages to your app.
					sendRegistrationIdToBackend();

					// For this demo: we don't need to send it because the
					// device will send
					// upstream messages to a server that echo back the message
					// using the
					// 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}

	private void registerUserInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "alertText",
						"Register Notification");
				KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "titleText", "Register");
				KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "contentText",
						"Registering Successful!");

				if (KeyValueAPI.isServerAvailable()) {

					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username, regid);

					msg = "User " + username + " registered, registration ID="
							+ regid;
				} else {
					msg = "Error :" + "Backup Server is not available";
					return msg;
				}

				// Persist the username - no need to register again.
				storeUsername(context, username);

				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}

	protected void RegisterIfUnregistered(int registrations) {

		String getString;
		boolean alreadyRegistered = false;
		for (int i = 1; i <= registrations; i++) {
			getString = KeyValueAPI.get(TEAM_NAME, TEAM_PASS,
					"regid" + String.valueOf(i));
			Log.d(String.valueOf(i), getString);
			if (getString.equals(regid))
				alreadyRegistered = true;
		}
		if (!alreadyRegistered) {
			KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "registrations",
					String.valueOf(registrations + 1));
			KeyValueAPI.put(TEAM_NAME, TEAM_PASS,
					"regid" + String.valueOf(registrations + 1), regid);
		}
	}

	/**
	 * Get number of registrations or initialize it if null
	 * 
	 * @return Number of registrations from KV server
	 */
	private int GetNumberOfRegistrations() {
		int registrations = 0;
		if (!KeyValueAPI.get(TEAM_NAME, TEAM_PASS, "registrations").contains(
				"Error")) {
			registrations = Integer.parseInt(KeyValueAPI.get(TEAM_NAME,
					TEAM_PASS, "registrations"));
		} else {
			KeyValueAPI.put(TEAM_NAME, TEAM_PASS, "registrations", "1");
			registrations = 1;
		}
		return registrations;
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		// Your implementation here.
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param userToStore
	 *            username
	 */
	private void storeUsername(Context context, String userToStore) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving username on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(USERNAME, userToStore);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.multiplayer_send_invite:
			sendInviteDialog();
			break;
		case R.id.multiplayer_start:
			startGameDialog();
			break;
		default:
			break;
		}
	}

	private void startGameDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (opponent.isEmpty()) {

			final TextView errMsg = new TextView(this);
			errMsg.setText("Send an invitiation to a user first!");
			builder.setTitle("No opponent!");
			builder.setView(errMsg);

			builder.setPositiveButton("OK", null);
			waitingForOpponent = false;
		} else {
			final TextView msg = new TextView(this);
			msg.setText("Waiting for opponent " + opponent + "...");
			builder.setTitle("Waiting...");
			builder.setView(msg);
			builder.setCancelable(false);

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							new AsyncTask<Void, Void, Void>() {
								@Override
								protected Void doInBackground(Void... arg0) {
									KeyValueAPI.put(TEAM_NAME, TEAM_PASS,
											username + "_READY", "false");
									return null;
								}
							}.execute(null, null, null);
							waitingForOpponent = false;
							dialog.cancel();
						}
					});

			waitingForOpponent = true;
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... arg0) {
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username + "_READY",
							"true");
					return null;
				}
			}.execute(null, null, null);
			waitForOpponent();
		}

		builder.show();
	}

	private void waitForOpponent() {
		new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected Boolean doInBackground(Void... arg0) {
				// TODO Auto-generated method stub
				while (waitingForOpponent) {

					String isOpponentReady = KeyValueAPI.get(TEAM_NAME,
							TEAM_PASS, opponent + "_READY");
					Boolean opponentReady = isOpponentReady.equals("true") ? true
							: false;
					KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username + "_ALIVE",
								"0");
					if (opponentReady) {
						waitingForOpponent = false;
						opponent_id = KeyValueAPI.get(TEAM_NAME, TEAM_PASS,
								opponent);
						
//						String win = KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username
//								+ "_LOST", "0");
						return true;
					}
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						waitingForOpponent = false;
						KeyValueAPI.put(TEAM_NAME, TEAM_PASS, username
								+ "_READY", "false");
						return false;
					}
				}
				return false;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				if (result) {
					startMultiplayerGame();
				}
			}

		}.execute(null, null, null);

	}

	protected void startMultiplayerGame() {
		Intent i = new Intent(this, MultiplayerGame.class);
		i.putExtra(OPPONENT, opponent);
		i.putExtra(OPPONENT_REG_ID, opponent_id);
		startActivity(i);

	}

	private void sendInviteDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Set up the input
		final EditText input = new EditText(this);
		builder.setTitle("Enter username to invite");
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String toUser = input.getText().toString();
				if (toUser.isEmpty()) {
					return;
				}
				opponent = toUser;

				TextView mOpponentView = (TextView) findViewById(R.id.multiplayer_opponent);
				mOpponentView.setText("Opponent: " + toUser);
				String fromUser = username;
				SendInvitationAsyncTask task = new SendInvitationAsyncTask(
						getApplicationContext(), regid);
				task.execute(toUser, fromUser);
				toastShort("Sending Invitation..");
			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	void setUserDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Set up the input
		final EditText input = new EditText(this);
		builder.setTitle("Set Username");
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", null);
		builder.setCancelable(false);

		final AlertDialog dialog = builder.create();

		// Persist the dialog until they pick a nonempty username
		dialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(final DialogInterface dialog) {

				Button b = ((AlertDialog) dialog)
						.getButton(AlertDialog.BUTTON_POSITIVE);
				b.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						username = input.getText().toString();
						if (username.isEmpty()) {
							toastShort("Choose a username!");
							return;
						}
						TextView mUsernameView = (TextView) findViewById(R.id.multiplayer_user);
						mUsernameView.setText("Username: " + username);
						registerUserInBackground();
						// Dismiss once everything is OK.
						dialog.dismiss();
					}
				});

			}
		});
		dialog.show();
	}

	void toastShort(String text) {
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
