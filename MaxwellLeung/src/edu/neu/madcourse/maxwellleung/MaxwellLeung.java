package edu.neu.madcourse.maxwellleung;

import edu.neu.madcourse.maxwellleung.sudoku.Sudoku;
import edu.neu.madcourse.maxwellleung.wordgame.WordGame;
import edu.neu.madcourse.maxwellleung.communication.Communication;
import edu.neu.madcourse.maxwellleung.dictionary.Dictionary;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import edu.neu.mobileClass.*;

public class MaxwellLeung extends Activity implements OnClickListener {
	private static final String TAG = "Main";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		//PhoneCheckAPI.doAuthorization(this);
		// Set up click listeners for all the buttons

		View aboutButton = findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);

		View generateErrorButton = findViewById(R.id.generate_error_button);
		generateErrorButton.setOnClickListener(this);

		View sudokuButton = findViewById(R.id.sudoku_button);
		sudokuButton.setOnClickListener(this);

		View dictionaryButton = findViewById(R.id.dictionary_button);
		dictionaryButton.setOnClickListener(this);

		View wordgameButton = findViewById(R.id.wordgame_button);
		wordgameButton.setOnClickListener(this);

		View communicationButton = findViewById(R.id.communication_button);
		communicationButton.setOnClickListener(this);
		
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);


	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.about_button:
			Intent i = new Intent(this, About.class);
			startActivity(i);
			break;
		// More buttons go here (if any) ...
		case R.id.generate_error_button:
			int n = 1 / 0;
			break;
		case R.id.sudoku_button:
			Intent s = new Intent(this, Sudoku.class);
			startActivity(s);
			break;
		case R.id.dictionary_button:
			Intent d = new Intent(this, Dictionary.class);
			startActivity(d);
			break;
		case R.id.wordgame_button:
			Intent f = new Intent(this, WordGame.class);
			startActivity(f);
			break;
		case R.id.communication_button:
			Intent g = new Intent(this, Communication.class);
			startActivity(g);
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		return false;
	}

}
