
package edu.neu.madcourse.maxwellleung;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.TextView;

public class About extends Activity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
      String imei = telephonyManager.getDeviceId();
      setContentView(R.layout.about);
      TextView aboutContent = (TextView) findViewById(R.id.about_content);
      aboutContent.setText(aboutContent.getText() + imei);
   }
}
