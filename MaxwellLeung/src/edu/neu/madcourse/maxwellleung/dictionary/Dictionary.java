package edu.neu.madcourse.maxwellleung.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import edu.neu.madcourse.maxwellleung.R;
import edu.neu.madcourse.maxwellleung.R.string;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class Dictionary extends Activity implements OnClickListener {

	private String[] DICTIONARY = { "app", "apple", "apples", "application",
			"appreciate" };

	private Set<String> dictionaryResults = new TreeSet<String>();

	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dictionary);

		// Set up click listeners for all the buttons
		View clearButton = findViewById(R.id.dictionary_clear);
		clearButton.setOnClickListener(this);
		View returnButton = findViewById(R.id.dictionary_return);
		returnButton.setOnClickListener(this);
		View acksButton = findViewById(R.id.dictionary_acks);
		acksButton.setOnClickListener(this);

		final MediaPlayer beep = MediaPlayer.create(this, R.raw.beep);
		
		final TextView dictionaryList = (TextView) findViewById(R.id.dictionary_list);
		// Input box listener
		EditText input = (EditText) findViewById(R.id.dictionary_input);
		input.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable e) {

				//Case insensitive and remove any spaces
				String textInput = e.toString().toLowerCase().replace(" ", "");
				int inputLength = textInput.length();
				if (inputLength == 1) {
					if (Character.isLetter(textInput.charAt(0))) {
						loadDictionary(textInput.charAt(0));
					}
				}
				// Do not show anything if 2 or less length
				if (inputLength <= 2) {

					dictionaryList.setText("");
				}

				if (inputLength > 2) {
					if (Arrays.asList(DICTIONARY).contains(textInput)) {
						dictionaryResults.add(textInput);
						beep.start();
					}
					dictionaryResults = filterResults(dictionaryResults,
							textInput);
					// Format results delimited by newline
					String results = formatResults(dictionaryResults);
					dictionaryList.setText(results);
				}
			}


			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	private void loadDictionary(char letter) {

		AssetManager assetManager = getAssets();
		try {

			ArrayList<String> inputList = new ArrayList<String>();
			String filename = "wordlist_" + letter + ".txt";
			InputStream input = assetManager.open(filename);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					input, "UTF-8"));
			String nextLine;
			try {
				while ((nextLine = reader.readLine()) != null) {
					inputList.add(nextLine);
				}
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			if (inputList.isEmpty()) {
				// throw new
				// Exception("List was empty when reading from file");
			}
			input.close();
			reader.close();

			DICTIONARY = (String[]) inputList.toArray(new String[inputList
					.size()]);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// txtContent.setText(text);
	}

	//Format the list delimited by newline
	private String formatResults(Set<String> result) {
		String str = "";
		for (Iterator<String> it = result.iterator(); it.hasNext();) {
			str += (it.next()) + "\n";
		}
		return str.toString();
	}

	// Filter out items in array if it contains the item
	private Set<String> filterResults(Set<String> arrayToFilter,
			String stringToCompare) {
		Iterator<String> it = arrayToFilter.iterator();
		while (it.hasNext()) {
			if (!stringToCompare.contains(it.next())) {
				it.remove();
				continue;
			}
		}
		return arrayToFilter;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.dictionary_clear:
			clearText();
			break;
		case R.id.dictionary_return:
			finish();
			break;
		case R.id.dictionary_acks:
			Intent i = new Intent(this, DictionaryAcknowledgements.class);
			startActivity(i);
			break;
		}

	}

	private void clearText() {

		EditText input = (EditText) findViewById(R.id.dictionary_input);
		input.setText("");
		final TextView dictionaryList = (TextView) findViewById(R.id.dictionary_list);
		dictionaryList.setText("");
	}
}