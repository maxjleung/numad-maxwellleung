package edu.neu.madcourse.maxwellleung.dictionary;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "dictionary";

	// Words table name
	private static final String TABLE_WORDS = "words";

	// Words Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_WORD = "word";

	public DBHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_WORDS_TABLE = "CREATE TABLE " + TABLE_WORDS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_WORD + " TEXT)";
		db.execSQL(CREATE_WORDS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORDS);

		// Create tables again
		onCreate(db);
	}

	// Adding new contact
	public void addWord(Word word) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_WORD, word.getWord());

		// Inserting Row
		db.insert(TABLE_WORDS, null, values);
		db.close(); // Closing database connection
	}

    // Getting single contact
    Word getWord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_WORDS, new String[] { KEY_ID,
                KEY_WORD }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        Word word = new Word(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));
        // return contact
        return word;
    }
     
    // Getting All Words
    public List<Word> getAllWords() {
        List<Word> wordList = new ArrayList<Word>();

        String selectQuery = "SELECT  * FROM " + TABLE_WORDS;
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Word contact = new Word();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setWord(cursor.getString(1));
                // Adding word to list
                wordList.add(contact);
            } while (cursor.moveToNext());
        }
 
        return wordList;
    }
 
    public int updateWord(Word word) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_WORD, word.getWord());

        // updating row
        return db.update(TABLE_WORDS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(word.getID()) });
    }
 
    public void deleteWord(Word word) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WORDS, KEY_ID + " = ?",
                new String[] { String.valueOf(word.getID()) });
        db.close();
    }
 
 
    public int getWordCount() {
        String countQuery = "SELECT  COUNT(*) FROM " + TABLE_WORDS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return Integer.parseInt(cursor.getString(0));
    }

}
