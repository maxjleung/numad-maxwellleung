package edu.neu.madcourse.maxwellleung.dictionary;

public class Word {
	
	int _id;
    String _word;
    
    // Empty constructor
    public Word(){
         
    }
    public Word(int id, String word){
    	this._id = id;
        this._word = word;
    }
    
    public int getID(){
        return this._id;
    }
     
    public void setID(int id){
        this._id = id;
    }
    
    public String getWord(){
        return _word;
    }
     
    public void setWord(String word){
        this._word = word;
    }
     
}
